# Reconciling high-performance computing with the use of third-party libraries?

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/datamove/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/datamove/-/commits/master)

High performance computing often requires relying on multiple software packages
and that these be optimized on the target machine where these computations run.
The optimization constraints are such that it is commonly accepted that the
deployment of this software can only be done manually or by relying on the work
of the target machine's administrators (typically via a load module). However,
the amount of work involved when doing it yourself or the unavailability of
precisely the required functionality when relying on modules provided by
administrators, means that many codes choose to provide themselves some
functionality that could be obtained via third party libraries, contrary to the
canons of software engineering.

In this talk, we will first review a quest (CMake, Spack, and now Guix) for an
environment to reliably deploy high-performance computing software in a
portable, high-performance, and reproducible manner so that the use of
third-party libraries is no longer a concern. We then dedicate the remaining
part of the presentation to a concrete example of a research study conducted
within an ongoing PhD thesis combining Guix [1] to ensure the reproducibility of
the software environment with Org mode [2] in an attempt to maintain an
exhaustive, clear and accessible description [3] of the experiments, the source
code and procedures involved in the construction of the experimental software
environment, the execution of benchmarks as well as the gathering and the
post-processing of the results.

**[View slides](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/datamove/datamove.pdf)**

## Local build

Clone the repository with its submodules.

```bash
git clone --recurse-submodules git@gitlab.inria.fr:thesis-mfelsoci/slides/datamove.git
```

Run the following command to build the PDF from `datamove.org`.

```bash
guix time-machine -C channels.scm -- shell --pure -m manifest.scm -- emacs \
    --batch --no-init-file --load publish.el --eval '(org-publish "datamove")'
```

## References

1. GNU Guix software distribution and transactional package manager
   [https://guix.gnu.org](https://guix.gnu.org).
2. Literate Programming, Donald E. Knuth, 1984
   [https://doi.org/10.1093/comjnl/27.2.97](https://doi.org/10.1093/comjnl/27.2.97).
3. The Org Mode 9.1 Reference Manual, Dominik Carsten, 2018.


